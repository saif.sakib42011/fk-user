import { productConstant } from "../actions/constants";

const initialState={
    products:[],
    productsByPrice:{
        under5k:[],
        unde10k:[],
        under15k:[],
        under20k:[],
        under30k:[],
    }
}
const productReducer=(state=initialState,action)=>{
    switch (action.type) {
        case productConstant.GET_PRODUCTS_BY_SLUG_SUCCESS :
            state={
                ...state,
                products:action.payload.products,
                productsByPrice:{
                    ...action.payload.productsByPrice
                }
            }
            break;
    
        default:
            
            break;
    }
    return state
    
}

export default productReducer