import React, {useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getProductsBySlug } from '../../actions'

import Layout from '../../components/layout'
import { generatePublicUrl } from '../../urlConfig'
import "./styles.css"


const ProductListPage = (props) => {
    //useState
    const [priceRange, setPriceRange] = useState({
        under5k:'under 5k',
        under10k:'under 10k',
        under15k:'under 15k',
        under20k:'under 20k',
        under30k:'under 30k',
    })
    //
    const product = useSelector(state => state.product)
    const dispatch = useDispatch()
    useEffect(() => {
        const { match } = props
        dispatch(getProductsBySlug(match.params.slug))
    }, [dispatch, props])
    return (
        <Layout>
            {
                Object.keys(product.productsByPrice).map((key,idx) => {
                    // console.log(key);
                    return <div key={idx} className="card">
                        <div className="cardHeader">
                            <div className='productaTitle'>{props.match.params.slug} Mobile {priceRange[key]}</div>
                            <button>View All</button>
                        </div>
                        
                        <div style={{display:'flex' }}>
                            {product.productsByPrice[key].map(prod=>{
                                console.log(prod);
                                return <div key={prod._id} className='productContainer'>
                                    <div className='productImgContainer'>
                                        <img src={generatePublicUrl(prod.productPictures[0].img)} alt={prod.productPictures[0].img} />
                                    </div>
                                    <div className='productInfo'>
                                        <div style={{ margin: '5px 0' }}>{prod.name}</div>
                                        <div>
                                            <span>4.3</span>
                                            <span>3343</span>
                                        </div>
                                        <div className='productPrice'>{prod.price}</div>
                                    </div>
                                </div>
                                }
                                )}

                        </div>
                    </div>

                })
            }
        </Layout>
    )
}

export default ProductListPage
