import React from 'react'
import Layout from '../../components/layout'


const HomePage = () => {
    return (
        <Layout>
            Homepage
        </Layout>
    )
}

export default HomePage
