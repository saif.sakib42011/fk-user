import React from 'react'
import Header from '../header'
import MenuHeader from '../menuHeader'

const Layout = ({children}) => {
    return (
        <>
           <Header/>
           <MenuHeader/>
           {children} 
        </>
    )
}

export default Layout
