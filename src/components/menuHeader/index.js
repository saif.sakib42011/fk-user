import React,{useEffect} from 'react'
import "./styles.css"
import {useDispatch,useSelector} from "react-redux"; 
import { getAllCategory } from '../../actions';
const MenuHeader = () => {
    const dispatch = useDispatch()
    const category = useSelector(state => state.category)
    useEffect(() => {
        dispatch(getAllCategory())
    }, [dispatch])
    const renderCategory = (categories) => {
        let myCategories = []
        for (let cat of categories) {
            myCategories.push(
                <li key={cat.name}>
                    {cat.parentId?<a href={cat.slug}>{cat.name}</a>:
                    <span>{cat.name}</span>
                }
                    
                    {cat.children.length > 0 ? (<ul>{renderCategory(cat.children)}</ul>) : null}
                </li>
            )
        }
        return myCategories
    }
    return (
        <div className="menuHeader">
            <ul>
                {renderCategory(category.categories)}
            </ul>
        </div>
    )
}

export default MenuHeader
