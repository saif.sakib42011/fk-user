import HomePage from "./container/homepage";
import { BrowserRouter as Router , Switch , Route } from "react-router-dom"; 
import ProductListPage from "./container/productListPage";

function App() {
  return (
    <Router>
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route  path="/:slug" component={ProductListPage}/>
        </Switch>
    </Router>
    
  );
}

export default App;
