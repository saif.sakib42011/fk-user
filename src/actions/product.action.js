import axios from "../helpers/axios"
import { productConstant } from "./constants";

export const getProductsBySlug=(slug)=>{
    return async dispatch =>{
        const res=await axios.get(`/api/product/${slug}`);
        console.log(res);
        if (res.status===200) {
            const {products,productsByPrice}=await res.data
            dispatch({  
                type:productConstant.GET_PRODUCTS_BY_SLUG_SUCCESS,
                payload:{products,productsByPrice}
            })
        }
        // else{
        //     dispatch({
        //         type:productConstant.GET_PRODUCTS_BY_SLUG_FAILURE,
        //         payload:res.status
        //     })
        // }
    }
}