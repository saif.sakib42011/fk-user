import axios from "../helpers/axios"
import { categoryConstant } from './constants'

export const getAllCategory = () => {
    return async (dispatch) => {

        const res = await axios.get('api/category/get')
        const { categoryList } = res.data
        
        await dispatch({
            type: categoryConstant.GET_ALL_CATEGORIES_REQUEST,  
        })

        if (res.status === 200) {
            await dispatch({
                type: categoryConstant.GET_ALL_CATEGORIES_SUCCESS,
                payload: { categories: categoryList }
            })
        } else {
            await dispatch({
                type: categoryConstant.GET_ALL_CATEGORIES_FAILURE,
                payload: { error: res.data.error }
            })
        }
    }
}

export const addCategory = (form) => {
    return async (dispatch) => {
        dispatch({
            type: categoryConstant.ADD_NEW_CATEGORY_REQUEST,
        })
        const res = await axios.post("api/category/create", form)
        
        if (res.status === 201) {
            dispatch({
                type: categoryConstant.ADD_NEW_CATEGORY_SUCCESS,
                payload: { category: res.data.category }
            })
        } else {
            dispatch({
                type: categoryConstant.ADD_NEW_CATEGORY_FAILURE,
                payload: { error: res.data.error }
            })
        }
    }
}